from flask import Flask, render_template, abort
from lxml import etree

app = Flask(__name__)

@app.route('/')
def pagina_principal():
    return render_template("pagina.html")

@app.route('/potencia/<int:numero1>/<numero2>')
def pagina_potencia(numero1,numero2):
    numero2=int(numero2)
    if numero2 > 0:
        return render_template("potencia.html",base=numero1,exponente=numero2,resultado=numero1**numero2)
    elif numero2 == 0:
        return render_template("potencia.html",base=numero1,exponente=numero2,resultado=1)
    elif numero2 < 0:
        return render_template("potencia.html",base=numero1,exponente=numero2,resultado=1/(numero1**-numero2))

@app.route('/cuentaletras/<palabra>/<letra>')
def pagina_cuentaletras(palabra,letra):
    if len(letra) > 1:
        abort(404)
    elif len(letra) == 1:
        return render_template("cuentaletras.html",palabra=palabra,letra=letra,veces=palabra.count(letra))

@app.route('/libro/<int:codigo>')
def pagina_libros(codigo):
    doc = etree.parse('libros.xml')
    return render_template("libros.html",codigo=codigo,libro=doc.xpath("/biblioteca/libro[codigo=%d]/titulo/text()" %codigo)[0],autor=doc.xpath("/biblioteca/libro[codigo=%d]/autor/text()" %codigo)[0])

app.run(debug=True)